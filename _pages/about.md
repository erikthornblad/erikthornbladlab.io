---
title: About me
permalink: /about/
---

I currently work as a Data Scientist at Qliro, a FinTech company in online payments. Before this I was a PhD student in mathematics at Uppsala University, and before this I did the MMath programme at Oxford University.

I enjoy programming in scala and python. I cannot say I enjoy programming in R, but I do it anyway. I love automating things. In terms of sports, I play badminton, squash and chess. I try to swim but struggle with the flip turn. I am also one of the organisers of the online chess league [lichess4545](https://lichess4545.com), a team league which sees around 250 players registering each season.


My Erdős number is 3[^1], and (depending on definitions) my Bacon number is either 3[^2] or infinity, bringing my Erdős-Bacon number to 6 or infinity.
sdfdsf

[^1]: Thörnblad ~ Stefansson ~ Janson ~ Erdős.

[^2]: Thörnblad ~ Tim Dutton ~ Anne Archer ~ Kevin Bacon. When I was a student, our chess club was asked to star in a TV pilo as "chess wranglers". Most likely nothing happened after the pilot was released, but it is difficult to forget the dialogue between the two main characters as they were playing a game of chess. 
    - Check!
    - Are you checking me out?