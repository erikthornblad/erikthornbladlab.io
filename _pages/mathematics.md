---
#layout: page
title: Mathematics
permalink: /mathematics/
---



In a previous life I was a PhD student and researcher in mathematics at Uppsala University, Sweden. My field was in the intersection between probability theory and combinatorics, with a focus on random graphs. I defended my [thesis](http://www.diva-portal.org/smash/record.jsf?pid=diva2%3A1175059&dswid=-8456) titled *Degrees in Random Graphs and Tournament Limits* in 2018. My advisor was Professor [Svante Janson](https://en.wikipedia.org/wiki/Svante_Janson). 


Preprints of all my papers are available on [arXiv](https://arxiv.org/search/math?searchtype=author&query=Th%C3%B6rnblad%2C+Erik). If you need access to the published version, please contact me directly.


10. Andrea Pasquali, Erik Thörnblad and Jakob Zimmermann. *Existence of symmetric maximal noncrossing collections of k-element sets.* 2018 
9. Erik Thörnblad and Jakob Zimmermann. *Counting Quasi-Idempotent Irreducible Integral Matrices.* Journal of Integer Sequences, Vol. 21 (2018), who was the Article 18.4.8. 
8. Erik Thörnblad. *Tournament limits: Score functions, degree distributions and self-converseness.* 2016 
7. Katja Gabrysch and Erik Thörnblad. *The greedy walk on an inhomogeneous Poisson process.* Electronic Communications in Probability, Vol. 23 (2018), who was the paper no. 14. 
6. Erik Thörnblad. *Eplett's theorem for self-converse generalised tournaments.* Australasian Journal of Combinatorics, 70:3 (2018), who was the 329-335. 
5. Erik Thörnblad. *Another proof of Moon's theorem on generalised tournament score sequences.* 
4. Erik Thörnblad. *Decomposition of tournament limits.* European Journal of Combinatorics, 67:96-125 (2018). 
3. Erik Thörnblad and Sigurdur Örn Stefansson. *Almost sure convergence of vertex degree densities in the vertex-splitting model.* Stochastic Models Vol. 32 Iss. 4 (2016) 289-305. 
2. Erik Thörnblad. *The dominating colour of an infinite Pólya urn model.* J. Appl. Probab. Volume 53, Number 3 (2016), who was the 914-924. 
1. Erik Thörnblad. *Asymptotic degree distribution of a duplication-deletion random graph model.* Internet Mathematics, (11) 2015, no 3, 289-305. 
{: reversed="reversed"}



The [mathematics genealogy project](https://www.genealogy.math.ndsu.nodak.edu) shows who you are related to in the tree of mathematicians (through student-advisor relationships). I managed to trace my mathematics genealogy tree back to the 13th century, by following 30 generations of mathematicians.

To put it plainly, Sharaf al-Dīn al-Ṭūsī (unknown), was the advisor of
Kamāl al-Dīn Ibn Yūnus (unknown), who was the advisor of
Nasir al-Dīn al-Ṭūsī (unknown), who was the advisor of
Shams al‐Dīn al‐Bukhārī (unknown), who was the advisor of 
Gregory Chioniadis (1296), who was the advisor of 
Manuel Bryennios (unknown), who was the advisor of 
Theodore Metochites (1315), who was the advisor of
Gregory Palamas (unknown), who was the advisor of
Nilos Kabasilas (1363), who was the advisor of
Demetrios Kydones (unknown), who was the advisor of 
Georgios Plethon Gemistos (1380, 1393), who was the advisor of
Basilios Bessarion (1436), who was the advisor of 
Janus Lascaris (1472), who was the advisor of 
Guillaume Budé (1486, 1491), who was the advisor of 
Jacques Toussain (1521), who was the advisor of
Petrus Ramus (1536), who was the advisor of 
Johannes Heurnius (1566), who was the advisor of
Pieter Pauw (1585), who was the advisor of 
Menelaus Winsemius (1613), who was the advisor of 
Johannes Antonides van der Linden (1630), who was the advisor of 
Petrus Hoffvenius (1660), who was the advisor of
Petrus Elvius (1688), who was the advisor of,
Anders Duhre (1711), who was the advisor of
Samuel Klingenstierna (1717), who was the advisor of 
Mårten Strömer (1731), who was the advisor of
Frederick Mallet (1752) the advisor of
Jöns Svanbert (1796), who was the the advisor of
Emanuel Björling (1830), who was the the advisor of
Cark Björling (1863), who was the the advisor of
Anders Wiman (1892), who was the the advisor of
Arne Beurling (1933), who was the the advisor of
Lennart Carleson (1950), who was the the advisor of
Svante Janson (1977, 1984), who was the the advisor of 
Erik Thörnblad (2018).