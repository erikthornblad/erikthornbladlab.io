---
#layout: page
title: Professional Experience
permalink: /professional_experience/
---

Since 2018 I have been working at Qliro, a fin-tech specialising in online-payments. 
I stared as data scientist working on credit risk modelling, but have since moved over to a more technical role. 
Today I am Credit Infrastructure Manager.

I believe strongly in automation and CI/CD and spend a good amount of time on thinking about how to build templates in Gitlab CI/CD to be used throughout different projects. This in combination with Docker and Docker Swarm (or alternatives) leads to zero-effort deployments. Apart from Docker, I use Python, Scala and R (the latter, reluctantly so) in most of my projects. Recently, most of my work has been focusing on setting up Airflow (on-prem). 