---
title:  Hosting a Jekyll website on GitLab
date:   2019-02-10
published: true
categories: [GitLab, Jekyll]
---

This website is powered through Jekyll using the theme Minimal-Mistakes. It is hosted on [GitLab]({{site.gitrepository}}) using GitLab pages. 

If you have a Jekyll setup working locally and want to host it on GitLab pages, all you need to do is to ensure the namespace of the repository is `username.gitlab.io`, activate GitLab pages and include a suitable `.gitlab-ci.yml`. I used the below `.gitlab-ci.yml`-file to build this webpage. The `UTF`-related variables are there to prevent some `html`-documents from parsing incorrectly and making the pipeline through an exception.

Whenever a commit is pushed to the master branch (`only master`), a GitLab runner will start and fetch the Docker image `ruby:2.3`. The variables specified are set as environment variables on the runner. The pages script will then trigger. If all steps succed, `html` artifacts are generated and the websit will be available at `https://username.gitlab.io`.

~~~
image: ruby:2.3

variables:
  JEKYLL_ENV: production
  LC_ALL: "C.UTF-8" 
  LANG: "en_US.UTF-8" 
  LANGUAGE: "en_US.UTF-8"
  
pages:
  script:
  - gem update --system
  - gem install bundler
  - bundler update --bundler
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master
~~~