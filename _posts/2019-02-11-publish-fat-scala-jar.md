---
title:  Publishing a fat scala jar with sbt publish
date:   2019-02-10
published: true
categories: [Scala, sbt]
---

A fat jar includes all dependencies for the project. The general recommendation seems to be to try to *avoid* publishing fat jars. However, for internal usage it can make sense.

To be able to publish a fat jar including all dependencies to an internally hosted repository, I had to add the following code block to my `build.sbt`.

{% highlight scala %}
artifact in (Compile, assembly) := {
  val art = (artifact in (Compile, assembly)).value
  art.withClassifier(Some("assembly"))
}

addArtifact(artifact in (Compile, assembly), assembly)
{% endhighlight %}

This is the suggested approach as given on [https://github.com/sbt/sbt-assembly](https://github.com/sbt/sbt-assembly/blob/v0.14.9/README.md). I already had `sbt-assembly` set up and an internal maven registry I could publish to. Whenever I now run `sbt publish`, the fat jar is published with a name ending in `-assembly.jar`.