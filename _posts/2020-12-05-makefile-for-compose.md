---
title:  Common docker-compose and Makefile
date:   2020-12-05
published: true
categories: [docker]
---

I normally use docker-compose, and to cut down on typing when developing, I usually include the following Makefile. If there are multiple dockerfiles in the same folder, instead use `docker-compose -f <docker-compose-file-name> xxx`.

```Makefile
make build:
    docker-compose build

make run:
    docker-compose run <service-name> 

make up:
    docker-compose up -d

make down:
    docker-compose down

make restart:
    make down
    make up

make logs:
    docker-compose logs

make exec:
	docker exec -it <service-name> /bin/bash
```