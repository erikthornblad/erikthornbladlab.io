---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
title: About me
author_profile: true
---

Welcome to my webpage! If you think it looks **great**, that's because I didn't write it. I have used [Jekyll](https://jekyllrb.com/), which is a great framework for static webpages, and the sleek design is due to [Michael Rose](https://mademistakes.com/).

On the blog I mainly post programming snippets I might need again in the future.

Most of my mathematics stuff I keep on the tab dedicated to [mathematics](./mathematics.md), but here is a tidbit: My Erdős number is 3[^1], and (depending on definitions) my Bacon number is either 3[^2] or infinity, bringing my Erdős-Bacon number to 6 or infinity.


[^1]: Thörnblad ~ Stefansson ~ Janson ~ Erdős.

[^2]: Thörnblad ~ Tim Dutton ~ Anne Archer ~ Kevin Bacon. When I was a student, our chess club was asked to star in a TV pilo as "chess wranglers". Most likely nothing happened after the pilot was released, but it is difficult to forget the dialogue between the two main characters as they were playing a game of chess. 
    - Check!
    - Are you checking me out?